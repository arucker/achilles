﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Achilles.Startup))]
namespace Achilles
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
